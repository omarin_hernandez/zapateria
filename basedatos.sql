-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 11, 2018 at 11:38 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `gestorZapateria`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`) VALUES
(1, 'omar@mail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `zapatos`
--

CREATE TABLE `zapatos` (
  `idZapato` int(11) NOT NULL,
  `nombreZapato` varchar(100) NOT NULL,
  `precioZapato` varchar(50) NOT NULL,
  `tallaZapato` varchar(50) NOT NULL,
  `colorZapato` varchar(50) NOT NULL,
  `imagenZapato` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zapatos`
--

INSERT INTO `zapatos` (`idZapato`, `nombreZapato`, `precioZapato`, `tallaZapato`, `colorZapato`, `imagenZapato`) VALUES
(1, 'Tennis nike', '1500', '27 1/2', 'azul', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zapatos`
--
ALTER TABLE `zapatos`
  ADD PRIMARY KEY (`idZapato`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `zapatos`
--
ALTER TABLE `zapatos`
  MODIFY `idZapato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
