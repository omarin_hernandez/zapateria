<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller{
    public function __construct(){
        parent:: __construct();
        $this->load->library('session');
    }
    public function index(){
        
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if ($email !=null) {
            
            $this->load->model('usersModel');
            if($this->usersModel->getUser($email, $password)){
                $this->load->view('inicio');//cargar vista inicio
                //redirect('zapateriaController');
            }
            else{
                $this->load->view('login');//cargar vista login
                //redirect('loginController');
            }

        }
        
    }
    public function cerrar(){
        $this->session->sess_destroy();
        $this->load->view('login');
    }
}
