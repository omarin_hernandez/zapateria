<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ZapateriaController extends CI_Controller {
    public function __construct(){
        parent:: __construct();
        $this->load->model('zapateriaModel');
        $this->load->library('session');
    }
    public function index(){
        $this->load->view('login');//cargar vista inicio
    }

    public function guardar(){
        if ($_POST) {
            $this->zapateriaModel->guardar($_POST);
        }
        $this->load->view('inicio');
    }
    public function borrar(){
        $this->zapateriaModel->borrar($_GET);
        $this->load->view('inicio');
    }
}
