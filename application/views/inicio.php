<?php
     $CI =& get_instance();
     if ($this->uri->segment(3)==0) {
         $zapato[0]['idZapato']="";
         $zapato[0]['nombreZapato']="";
         $zapato[0]['precioZapato']="";
         $zapato[0]['tallaZapato']="";
         $zapato[0]['colorZapato']="";
     }else{
         $CI->db->where('idZapato', $this->uri->segment(3));
         $zapato=$CI->db->get('zapatos')->result_array();
     }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ZAPATERIA - OM@R</title>
<!-- BOOTSTRAP -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

 <style>
    
 </style>

</head>
<body>

          
   <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>ZAPATERIA OMAR</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">AGREGAR ZAPATOS</div>
                    <div class="panel-body">

                        <form action="<?php echo base_url('zapateriaController/guardar')?>" method="post">
                            <!-- Oculto -->
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Id</label>
                                <input type="text" name="idZapato" class="form-control" value="<?=$zapato[0]['idZapato']?>"readonly>
                            </div>
                            <!-- Oculto-->
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Nombre</label>
                                <input required type="text" name="nombreZapato" class="form-control" value="<?=$zapato[0]['nombreZapato']?>">
                            </div>

                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Precio $</label>
                                <input required type="number" name="precioZapato" class="form-control" value="<?=$zapato[0]['precioZapato']?>">
                            </div>

                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Talla</label>
                                <input required type="text" name="tallaZapato" class="form-control"value="<?=$zapato[0]['tallaZapato']?>">
                            </div>

                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Color</label>
                                <input required type="text" name="colorZapato" class="form-control" value="<?=$zapato[0]['colorZapato']?>">
                            </div>

                            <div class="col-md-12 text-center">
                            <button type="submit" class="  btn btn-success">Guardar Zapato</button>
                            <a href="<?php echo base_url("zapateriaController/guardar")?>" class="btn btn-primary">Nuevo Zapato</a>
                            </div>
                            
                        </form>
                    
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">ZAPATOS AGREGADOS</div>
                    <div class="panel-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Talla</th>
                                <th>color</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?php 
                                    $CI =& get_instance();
                                    $zapatos = $CI->db->get('zapatos')->result_array();//select * from zapatos

                                    foreach ($zapatos as $zapato) {
                                        $rutaEditar = base_url("ZapateriaController/guardar/{$zapato['idZapato']}");
                                        $rutaBorrar = base_url("ZapateriaController/borrar?borrar={$zapato['idZapato']}");
                                        echo "
                                            <tr>
                                                <td>{$zapato['idZapato']}</td>
                                                <td>{$zapato['nombreZapato']}</td>
                                                <td>{$zapato['precioZapato']}</td>
                                                <td>{$zapato['tallaZapato']}</td>
                                                <td>{$zapato['colorZapato']}</td>

                                                <td>
                                                    <a href='{$rutaEditar}' class='btn btn-info'>Editar </a>
                                                    <a href='{$rutaBorrar}' onclick = 'return confirm(\"Estas Seguro?\")' class='btn btn-danger'>Borrar</a>
                                                </td>
                                            </tr>
                                        ";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
   </div>
</body>
<p><a href = "<?= base_url()?>loginController/cerrar">cerrar session</p>
</html>