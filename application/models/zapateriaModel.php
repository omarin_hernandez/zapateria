<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ZapateriaModel extends CI_Model {
    public function __construct(){
        parent:: __construct();
    }
    public function guardar($post){
        $datosZapato = array();
        $datosZapato['idZapato']=$post['idZapato'];
        $datosZapato['nombreZapato']=$post['nombreZapato'];
        $datosZapato['precioZapato']=$post['precioZapato'];
        $datosZapato['tallaZapato']=$post['tallaZapato'];
        $datosZapato['colorZapato']=$post['colorZapato'];
    
        if ($datosZapato['idZapato'] > 0) {
            $this->db->where('idZapato', $datosZapato['idZapato']);
            $this->db->update('zapatos', $datosZapato);
            $ruta=base_url('zapateriaController');

        echo "<script> 
                alert('Zapato Modificado con Exito...');
                windows.location='{$ruta}';
            </script>";
        }else{
            $this->db->insert('zapatos',$datosZapato);
            $ruta=base_url('zapateriaController');

            echo "<script> 
                    alert('Zapato Guardado con Exito...');
                    windows.location='{$ruta}';
                </script>";
        }
    }
    public function borrar($get){
        $this->db->where('idZapato', $get['borrar']);
        $this->db->delete('zapatos');
    }
   
}   